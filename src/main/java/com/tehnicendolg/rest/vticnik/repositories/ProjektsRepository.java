package com.tehnicendolg.rest.vticnik.repositories;
import com.tehnicendolg.rest.vticnik.models.Projekts;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
public interface ProjektsRepository extends MongoRepository<Projekts, String> {
    Projekts findBy_id(ObjectId _id);
}