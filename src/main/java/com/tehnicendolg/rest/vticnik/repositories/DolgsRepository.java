package com.tehnicendolg.rest.vticnik.repositories;
import com.tehnicendolg.rest.vticnik.models.Dolgs;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
public interface DolgsRepository extends MongoRepository<Dolgs, String> {
    Dolgs findBy_id(ObjectId _id);
}