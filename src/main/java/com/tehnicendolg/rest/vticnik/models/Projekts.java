package com.tehnicendolg.rest.vticnik.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.Date;

public class Projekts {
    @Id
    public ObjectId _id;

    public String naziv;
    public String clan;
    public String clan1;
    public String clan2;
    public String clan3;
    public String clan4;
    public String clan5;
    public String clan6;
    public String clan7;
    public String clan8;
    public String clan9;
    public String tip_ocena;
    public String zgornja_meja_dolga;
    public Date datum;
    public String error;

    // Constructors
    public Projekts() {}

    public Projekts(ObjectId _id, String naziv, String clan, String clan1,
                   String clan2, String clan3, String clan4, String clan5,
                   String clan6, String clan7, String clan8, String clan9,
                   String tip_ocena, String zgornja_meja_dolga, Date datum,
                   String error) {
        this._id = _id;
        this.naziv = naziv;
        this.clan = clan;
        this.clan1 = clan1;
        this.clan2 = clan2;
        this.clan3 = clan3;
        this.clan4 = clan4;
        this.clan5 = clan5;
        this.clan6 = clan6;
        this.clan7 = clan7;
        this.clan8 = clan8;
        this.clan9 = clan9;
        this.tip_ocena = tip_ocena;
        this.zgornja_meja_dolga = zgornja_meja_dolga;
        this.datum = datum;
        this.error = error;
    }

    // ObjectId needs to be converted to string
    public String get_id() { return _id.toHexString(); }
    public void set_id(ObjectId _id) { this._id = _id; }

    public String getNaziv() { return naziv; }
    public void setNaziv(String naziv) { this.naziv = naziv; }

    public String getClan() { return clan; }
    public void setClan(String clan) { this.clan = clan; }

    public String getClan1() { return clan1; }
    public void setClan1(String clan1) { this.clan1 = clan1; }

    public String getClan2() { return clan2; }
    public void setClan2(String clan2) { this.clan2 = clan2; }

    public String getClan3() { return clan3; }
    public void setClan3(String clan3) { this.clan3 = clan3; }

    public String getClan4() { return clan4; }
    public void setClan4(String clan4) { this.clan4 = clan4; }

    public String getClan5() { return clan5; }
    public void setClan5(String clan5) { this.clan5 = clan5; }

    public String getClan6() { return clan6; }
    public void setClan6(String clan6) { this.clan6 = clan6; }

    public String getClan7() { return clan7; }
    public void setClan7(String clan7) { this.clan7 = clan7; }

    public String getClan8() { return clan8; }
    public void setClan8(String clan8) { this.clan8 = clan8; }

    public String getClan9() { return clan9; }
    public void setClan9(String clan9) { this.clan9 = clan9; }

    public String getTipOcena() { return tip_ocena; }
    public void setTipOcena(String tip_ocena) { this.tip_ocena = tip_ocena; }

    public String getZgornjaMejaDolga() { return zgornja_meja_dolga; }
    public void setZgornjaMejaDolga(String zgornja_meja_dolga) { this.zgornja_meja_dolga = zgornja_meja_dolga; }

    public Date getDatum() { return datum; }
    public void setDatum(Date datum) { this.datum = datum; }

    public String getError() { return error; }
    public void setError(String error) { this.error = error; }

}
