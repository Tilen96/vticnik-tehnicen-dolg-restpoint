package com.tehnicendolg.rest.vticnik;

import com.tehnicendolg.rest.vticnik.models.Projekts;
import com.tehnicendolg.rest.vticnik.repositories.ProjektsRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;
@RestController
@RequestMapping("/projekts")

public class ProjektsController {

    @Autowired
    private ProjektsRepository repository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Projekts> getAllProjekt() {
        return repository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Projekts getProjektById(@PathVariable("id") ObjectId id) {
        return repository.findBy_id(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void modifyProjektById(@PathVariable("id") ObjectId id, @Valid @RequestBody Projekts projekts) {
        projekts.set_id(id);
        repository.save(projekts);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Projekts createProjekt(@Valid @RequestBody Projekts projekts) {
        projekts.set_id(ObjectId.get());
        repository.save(projekts);
        return projekts;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteProjekt(@PathVariable ObjectId id) {
        repository.delete(repository.findBy_id(id));
    }

}
