package com.tehnicendolg.rest.vticnik;
import com.tehnicendolg.rest.vticnik.models.Dolgs;
import com.tehnicendolg.rest.vticnik.repositories.DolgsRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;
@RestController
@RequestMapping("/dolgs")

public class DolgsController {

    @Autowired
    private DolgsRepository repository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Dolgs> getAllDolg() {
        return repository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Dolgs getDolgById(@PathVariable("id") ObjectId id) {
        return repository.findBy_id(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void modifyDolgById(@PathVariable("id") ObjectId id, @Valid @RequestBody Dolgs dolgs) {
        dolgs.set_id(id);
        repository.save(dolgs);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Dolgs createDolg(@Valid @RequestBody Dolgs dolgs) {
        dolgs.set_id(ObjectId.get());
        repository.save(dolgs);
        return dolgs;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteDolg(@PathVariable ObjectId id) {
        repository.delete(repository.findBy_id(id));
    }

}
